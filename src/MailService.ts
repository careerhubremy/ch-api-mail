import nodemailer, { Transporter } from 'nodemailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';

class MailService {
	private static instance: MailService;

	private mailer: Transporter;

	private constructor() {
		this.mailer = nodemailer.createTransport({
			host: "smtp.mailtrap.io",
			port: 2525,
			auth: {
				user: "41a2f91f52cf2b",
				pass: "65768fa8f4d7b8"
			}
		});
	}

	public static getInstance(): MailService {
		if (!MailService.instance) {
			MailService.instance = new MailService();
		}

		return MailService.instance;
	}

	public sendMessage(message: object) {
		this.mailer.sendMail(message);
	}
}
export default MailService;