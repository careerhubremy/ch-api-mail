import { compile } from 'handlebars'
import { readFileSync } from 'fs'
import { join } from 'path'

export const generate = (path) => compile(readFileSync(join(__dirname, path), "utf8"))
