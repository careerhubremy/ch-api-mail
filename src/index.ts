import dotenv from 'dotenv'
import { json } from 'body-parser'
dotenv.config()
import polka from "polka"
import { generate } from './HandlebarsCompiler'
import shared from 'ch-api-ts-shared'
import MailService from './MailService'
const server = polka();
const mailService = MailService.getInstance();

//Middleware
server.use(shared.logger)
server.use(json())
//Routes
server.post("/api/confirm-link", (req, res) => {
	try {
		const { to, subject, link } = req.body;
		const template = generate("./templates/invitation.hbs");
		mailService.sendMessage({
			from: "careerhub@gmail.com",
			to: to,
			subject: subject || "CareerHub invitation link.",
			html: template({ link: link })
		})

		res.end(JSON.stringify({
			status: "SUCCESS",
			result: `Email to ${to} successfully sent.`
		}))
	}
	catch (error) {
		res.writeHead(500, {
			'Content-Type': 'application/json'
		});

		res.end(JSON.stringify({ status: "ERROR", result: `${error}` }))
	}
})

server.listen(8080, () => {
	console.log("Mail service is runing on http://localhost:8080/");
})